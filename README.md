# My backup script
**This is my backup script using restic and summon**

## Prerequisites
1. gopass
2. summon
3. restic

## How to use it
### 1. You have to add an entry to your gopass like that ->
```Yaml
aws_access_key_id: myaccesskey
aws_secret_access_key: mysecretkey
restic_password: myresticpass
restic_repository: s3:https://s3.mycompany.com/my-bucket
```

### 2. You must modify and adapt all the files located in the config directory.

### 3. You have to run the install script
```Shell
bash ./install
```

## Uninstall
```Shell
bash ./uninstall
```

## Usage
```
Usage:
  backups3 [command]

Available Commands:
  backup        Create a new backup of files and/or directories
  check         Check the repository for errors
  diff          Show differences between two snapshots
  forget        Remove snapshots from the repository
  help          Help about any command
  init          Initialize a new repository
  list          List objects in the repository
  prune         Remove unneeded data from the repository
  restore       Extract the data from a snapshot
  stats         Scan the repository and show basic statistics
  unlock        Remove locks other processes created
```

## Notes
The gopass wrapper for summon comes from here : [https://dev.to/camptocamp-ops/simple-secret-sharing-with-gopass-and-summon-40jk](https://dev.to/camptocamp-ops/simple-secret-sharing-with-gopass-and-summon-40jk)
